# EPFL PhD thesis template with LHCb format 

Unofficial EPFL Thesis template with LHCb predefined commands. 
Provisional title **THESIS-TITLE**.

The thesis is organised as following 
1. `my_thesis.tex` manages the document
2. `head/` loads packages and defines title (in `title_sample`), front page, abstract and, acknowledgement. 
3. `main/` main part of the thesis.
4. `figures` where your figures are.
5. `appendix/`
6. `tail/` contains bib and cv.
7. `images/` is just for EPFL logo.
8. `utopia_font/`. the template is based on utopia font, in case you don't have. 

The document is also compiled in [https://www.overleaf.com/read/fgbdktpskwcb](https://www.overleaf.com/read/fgbdktpskwcb) - you may need to Recompile from scratch.

Maintenance or contributions from anyone are very welcome. 

# Reference 

This thesis is based on 
- [EPFL PolyDoc Unofficial Thesis template](https://github.com/glederrey/EPFL_thesis_template) 
- [LHCb docs template](https://gitlab.cern.ch/lhcb-docs/templates)
