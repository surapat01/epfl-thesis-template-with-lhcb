\chapter{Experimental environment\label{sec:ExpEnv}}
% LHCB-DP-2014-002 LHCb Detector Performance
% LHCb-DP-2008-001 The LHCb detector
% Fact https://twiki.cern.ch/twiki/bin/view/Main/LHCb-Facts
\graphicspath{{./figures/Exp_Environment/20220522/}}

This thesis is based on the dataset collected by the \lhcb detector 
during the operation of the Large Hadron Collider (LHC) from 2016 to 2018. 
A brief overview of the LHC and details about the detector are summarised in this chapter. 


\section{The Large Hadron Collider at CERN}

%[ LHC definition ]
The European Organization for Nuclear Research (\textit{Conseil européen pour la recherche nucléaire}, CERN) 
is a research organisation hosting the current largest particle physics laboratory in the world. 
It operates particle accelerators and computing infrastructures for research in the field. 

\begin{figure}[bth]
	\centering
		\includegraphics[width=1.00\textwidth]{CCC-v2018-print-v2.pdf} 
% CITE https://cds.cern.ch/record/2636343?ln=fr 
    \caption{The CERN accelerator complex as of August 2018~\cite{CERNAcc}.}
	\label{Fig:AccComplex}
\end{figure}


%[ LHC accelerator complex ]
The largest current accelerator is the Large Hadron Collider (LHC) situated in the suburbs of Geneva on the Swiss-Franco border. 
It is part of the CERN accelerator complex as shown in~\cref{Fig:AccComplex}.
The main purpose of the LHC is to explore and study predictions of different theories in particle physics. 
It is a hadron synchrotron accelerator, located in a circular tunnel of 27 kilometres of circumference.
The machine is operating around 100 metres below the surface to shield harmful radiation with the 
earth's crust~\cite{CERNBrochure}. 
The LHC is designed to accelerate two proton beams and collide them at a maximum energy of 14 TeV 
in the centre of mass~\cite{LHCRestart}. 
To reach this high energy, the beams are progressively accelerated by a series of particle accelerators (as of August 2018):
\begin{itemize}
    \setlength\itemsep{0.5em}
    \item The Linear accelerator (LINAC) 2 boosts protons extracted from hydrogen gas to 50 \mev. 
    \item The Proton Synchrotron Booster (Booster) accelerates the protons up to 1.4 \gev. 
    \item The Proton Synchrotron (PS) accelerates them further to 25 \gev.
    \item The Super Proton Synchrotron (SPS) gives the protons an energy of 450 \gev. 
    \item The LHC, fed with protons from the SPS in two counter-rotating beams, provides the final energy. 
\end{itemize}
The LHC operates with a nominal number of proton bunches of 2808 per beam, where each bunch consists of about $10^{11}$ protons. 
Once the protons have been accelerated to the required energy, the two counter-rotating beams are focused to collide at 
four collision points where the \alice, \atlas, \cms, and \lhcb detectors are located.

An important characteristics of particle accelerators is the instantaneous luminosity. 
It is defined as the rate of produced events
of a certain type divided by the corresponding production cross-section.
The integral of the instantaneous luminosity over time is known as the integrated luminosity.
This quantity determines the size of the collected data sample available for analysis. 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage

\begin{figure}[!t]
	\centering
		\includegraphics[width=0.90\textwidth]{LHCbDetector.png} 
% CITE LHCb-DP-2008-001
    \caption{Schematic side view of the LHCb detector~\cite{LHCb-DP-2008-001}.}
	\label{Fig:LHCbdet}
\end{figure}

\section{The LHCb detector}

The LHCb detector is a single-arm forward spectrometer dedicated to search for new physics interactions in 
the decay of particles containing a beauty or charm quark. The detector covers a region close to the LHC beam pipe.
The geometrical acceptance is expressed in terms of the pseudorapidity
\begin{equation}
    \eta = -\ln\left[\tan\left(\frac{\theta}{2}\right)\right],
\end{equation}
where $\theta$ is the angle between the momentum of a produced particle and the beam axis ($z$ axis). 
The LHCb detector covers the range $2 < \eta < 5$, which is optimised for $b$-hadron production at the LHC. 
The $b\overline{b}$ production cross-section in this region is higher than in the range $0 < \eta < 2$. 
After a quark is produced, it hadronises to form hadrons due to colour confinement which prevents a quark to exist individually. 


The distinguishing feature of the experiment is the choice to operate at low pile-up with almost constant 
luminosity during the data-taking period. This is controlled by 
a luminosity levelling procedure~\cite{Follin:2014nva}.   
The pile-up is defined as the average number of simultaneous collisions in a bunch crossing. 
At the \lhcb detector, this is around 3 depending on the year and running conditions. 
Operating at low pile-up simplifies event topology and avoids confusion caused by 
a large number of overlapping interactions. 
This leads to a clean event reconstruction and more efficient computing performance. 
\cref{Fig:LHCbdet} presents a sketch of the LHCb detector. It is composed of different subdetectors forming the tracking and 
particle identification systems.

The LHCb detector collected available data for physics analysis during \textit{Run 1} (2010--2012) at a 
centre-of-mass energy of $\sqrt{s} = 7-8 \tev$ and \textit{Run 2} (2015--2018) at $\sqrt{s} = 13 \tev$. 
The corresponding luminosity is reported in~\cref{Fig:LHCbLumi}.

\begin{figure}[!t]
	\centering
		\includegraphics[width=0.90\textwidth]{IntegratedLumiLHCbTime_Yearly.png} 
    \caption{Integrated luminosity as recorded by the \lhcb detector~\cite{OpsPlot}. }
	\label{Fig:LHCbLumi}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nocite{SEkInMaster}

\subsection{Tracking system}

The LHCb tracking system consists of the VErtex LOcator (VELO), the LHCb dipole magnet, and four tracking stations. 
The VELO is placed close to the collision point. A first tracking station, Tracker Turicensis (TT), is situated upstream 
of the dipole magnet and three tracking stations (T1, T2, T3) are placed downstream. These three stations use silicon 
microstrip detectors as their inner tracker (IT), which is the same technology as for the VELO and TT, while 
straw-tubes are used in the outer tracker (OT). 


\underline{Vertex Locator (VELO)}

The VErtex LOcator (VELO) is a silicon micro-strip detector placed around the collision point.
It covers $\sim 1$ metre along the beam line.
It aims at a precise reconstruction of the primary vertices (PV) and secondary vertices (SV) in the event. 
The precise identification of these vertices are crucial as the $b$- and $c$-hadrons are formed at the PV and decay at the SV.
The VELO is designed as two sets of semicircular sensors perpendicular to the beam. 
It is built from 21 modules to measure the azimuthal 
angle ($\Phi$) and radial distance ($R$) of a particle transversing the sensors.  

The detector can be operated in two configurations. During a data acquisition run, the two parts 
are moved toward each other, leaving an aperture in the centre of the VELO for the LHC proton beam.
The closest channels of the detectors are about 8 mm from the beamline.
To reduce radiation damage and prolong the lifetime of the detector, these two halves are separated outside of the 
data acquisition runs~\cite{LHCb-TDR-005,LHCbVELOWeb}. 
\begin{figure}[!bt]
	\centering
		\includegraphics[width=0.850\textwidth]{Velo.pdf} 
    \caption{Layout of the VELO circular modules~\cite{LHCb-DP-2008-001}.}
	\label{Fig:Velo}
\end{figure}

\underline{Tracking stations}

The tracking stations are modules to track the passage of a particle traversing the detector. 


\begin{minipage}[H]{0.60\textwidth}
    \textbf{The Tracker Turicensis (TT)} 
    consists of four silicon micro-strip layers with an active area of 8.4 m$^2$, covering
    between 2.0 and 4.9 in pseudorapidity. The system is held in an insulated volume at 5 \degc. 
    The four layers are rotated in the $xy$ plane by 
    $0^\circ$, $+5^\circ$, $-5^\circ$, and $0^\circ$ with respect to the vertical direction 
    as shown in~\cref{Fig:TTLayout}, in order to enable stereo reconstruction. 
    This system provides a measurement of the momentum $\vec{P}$ of charged particles through their curvature. 
    The momentum resolution depends on hit and track reconstruction as well as the magnetic field. 
\end{minipage}\hfil
\begin{minipage}[H]{0.35\textwidth}
  \centering
    \raisebox{-0.42\height}{%
  \includegraphics[width=\textwidth]{TT-Layout.pdf}
    }
  \captionof{figure}{Layout of the Tracker Turicensis~\cite{LHCb-DP-2018-003}.}
  \label{Fig:TTLayout}
\end{minipage}

\begin{figure}[t]
	\centering
		\includegraphics[width=0.420\textwidth]{InnerTrackerSch.pdf} \hfil
		\includegraphics[width=0.420\textwidth]{OTSch.pdf}  \\
    \caption{Layout of the (left) Inner Tracker~\cite{LHCb-DP-2008-001} and (right) Outer Tracker~\cite{LHCb-DP-2013-003}.}
	\label{Fig:ITFront}
\end{figure}
\textbf{The Inner Tracker (IT) and Outer Tracker (OT) stations} 
are the three downstream tracking modules (T1--T3) after the \lhcb magnet, as presented in~\cref{Fig:LHCbdet}. 
The IT uses silicon micro-strip layers like the TT. 
It is built close to the beam pipe where the occupancy of 
charged particles is large. The IT detects
around 20\% of the charged particle passing through the stations while covering only 1.3\% of the total surface area. 
The geometrical acceptance of this region is $3.4 < \eta < 5.0$ in the $xz$ plane and $4.5 < \eta < 4.9$ in the $yz$ plane.
On the other hand, the OT is constructed around the IT. It is a drift tube detector with a mixture of 
Argon (70.0\%), \cotwo (28.5\%), and \Otwo (1.5\%) gas. Each tracking station is a combination of four layers of IT and OT modules
with orientations similar to the ones in TT. 
\cref{Fig:ITFront} shows the configurations of IT and OT.  


\underline{The LHCb dipole magnet}

The charge and momentum of a particle determine its curvature under the influence of a magnetic field.
At the \lhcb detector, a non-superconducting dipole magnet is used and placed between TT and T1. 
It generates an integrated magnetic field of 4 Tm for tracks of 10 m length in the $xz$ plane. 
The field is mainly vertical and the polarity of the magnet is inverted regularly (with 
configurations called MagUp and MagDown) during the data acquisition, to study
the instrumental asymmetries as the performance of the detector is not symmetric along the $x$ axis. 
\cref{Fig:MagnetSch} shows the \lhcb magnet as well as the vertical field along the beam axis.  

\begin{figure}[bth]
	\centering
		\includegraphics[width=0.46\textwidth]{Magnet_Sch.pdf} \hfil 
		\includegraphics[width=0.43\textwidth]{MagnetTrackType.png} \\
    \caption{(Left) Drawing of the \lhcb dipole magnet. (Right) Vertical magnetic field in the MagDown configuration at the location of the different tracking stations. The different track types are also shown~\cite{LHCb-DP-2008-001}.}
	\label{Fig:MagnetSch}
\end{figure}

\underline{Track reconstruction}

Different types of tracks are reconstructed in the $\lhcb$ detector as shown in~\cref{Fig:MagnetSch}: 
\begin{itemize}
    \setlength\itemsep{0.5em}
    \item A VELO track is reconstructed from VELO hits only.  
    \item A Long track is reconstructed in the whole tracking system. 
    \item An Upstream track is similar to a Long track but the track is bent out of the detector before the three tracker stations.
    \item A Downstream track is similar to a Long track, but the track is not seen in the VELO. 
    \item A T track is reconstructed using only hits in the three downstream tracking stations. 
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Particle identification}

The unique fundamental requirement of the \lhcb detector is the Particle IDentification (PID) to distinguish different particle 
types from their interactions in different parts of the detector. This PID system is composed of two  
Ring Imaging Cherenkov Detectors (RICH), an electromagnetic calorimeter (ECAL), a hadronic calorimeter (HCAL), and muon stations. 

\underline{Ring Imaging Cherenkov Detectors (RICH)}

\begin{figure}[!t]
	\centering
		\includegraphics[width=0.40\textwidth]{CherenkovRad.pdf} 
		\includegraphics[width=0.27\textwidth]{RICH1.pdf} 
		\includegraphics[width=0.27\textwidth]{RICH2.pdf} 
    \caption{(Left) Cherenkov angle as a function of the particle momentum in aerogel, \cffour, and \cfourften. Designs of (middle) RICH1 and (right) RICH2. Figures taken from Ref.~\cite{LHCb-DP-2008-001}.}
	\label{Fig:RICH12}
\end{figure}

The RICH detectors are photodetectors constructed to capture Cherenkov radiation from high energy particles 
propagating faster than the speed of light in a medium. The relation between the particle velocity, 
$\beta c$, and the angle of Cherenkov light with respect to the particle's velocity $\theta_C$ is 
%
\begin{equation}
	\beta = \frac{1}{n\cos\theta_C} \ ,
\end{equation}
%
where $n$ is the refractive index of the medium. The information obtained from the RICH detectors plays an 
important role in particle identification, since charged particles with different masses lead to 
different relations between the Cherenkov angle and the particle momentum, as shown in~\cref{Fig:RICH12} (left).
To optimise for the sensitivity in distinguishing kaons from pions 
for the whole momentum spectrum, two RICH detectors are deployed. 
One is located after the VELO (RICH1) and another one after the downstream trackers (RICH2), as shown in~\cref{Fig:LHCbdet}. 
RICH1 covers momenta ranging from $\sim1$ to $60 \gevc$ with \cfourften radiator 
(the aerogel was removed before \textit{Run 2} started). 
\cffour radiator is used in RICH2 to cover the high momentum range from $\sim 15\gevc$ up to more than 100 \gevc. 



\begin{figure}[!t]
    \begin{minipage}{6in}
        \centering
        \raisebox{-0.5\height}{\includegraphics[width=0.45\textwidth]{CalLayout.pdf} }
        \hfil
        \raisebox{-0.42\height}{\includegraphics[width=0.45\textwidth]{CalSig.pdf} }
        %\caption{(Left) Schematic view of the calorimeter system~\cite{LHCb-DP-2020-001}. (Right) Signatures of an electron $e$, a hadron $h$, and a photon \gamma~in the calorimeter system~\cite{PicatosteOlloqui:2009xwb}.}
        \caption{ check}
	    \label{Fig:CalLayout}
    \end{minipage}
\end{figure}

\underline{Calorimeters}

The calorimeter system is designed to absorb particles and measure their energies.  
Photons, electrons and hadrons can be identified by the calorimeter system, which consists of a scintillating 
pad detector (SPD), a preshower detector (PS), an electromagnetic calorimeter (ECAL) and a hadronic calorimeter (HCAL)
as shown in~\cref{Fig:CalLayout} (left). 

The SPD and PS are made of scintillators. They provide fast particle identification and background 
rejection during online data acquisition. The SPD is placed in front of the PS separated by a lead converter.  
Particles interacting electromagnetically lose their energy by inducing electromagnetic showers in the ECAL. 
Hadrons, which can interact through the strong interaction, deposit their energy in the hadronic calorimeter (HCAL). 
The ECAL (HCAL) is arranged with alternating layers of scintillator and lead (iron).    
The signature of each long-lived particle (electron $e$, hadron $h$, and photon) in terms of energy 
deposition in the calorimeter system is shown in~\cref{Fig:CalLayout} (right).


\begin{wrapfigure}{r}{.4\textwidth}
	\centering
		\includegraphics[width=0.90\textwidth]{MuonStation.pdf} 
% CITE LHCb-DP-2020-001
    \caption{Layout of the muon stations~\cite{LHCb-DP-2008-001}.}
	\label{Fig:MuLayout}
\end{wrapfigure}
\underline{Muon system}

The main process for an electron to produce an electromagnetic shower in the calorimeters is Bremsstrahlung.
It is electromagnetic radiation emitted when a charged particle is accelerated. 
The emission power is inversely proportional to the square of the mass of the charged particle. 

Since the muon mass is 200 times the electron mass, a muon deposits much less energy than electrons in the calorimeters. 
It is also a lepton, insensitive to the strong interaction. 
Muon chambers are placed at the very end of the detector to detect muons. 
In \lhcb, these chambers are composed of alternating layers of iron and multiwire proportional chambers to 
detect ionisation of the gas inside the chambers. The chambers form five stations (M1--M5) located after 
the calorimeters except M1, as shown in~\cref{Fig:MuLayout}. 

The triple gas electron multiplier (GEM) technology is used to cope with the harsh environment close to the beam pipe 
in the M1 station due to high particle rate. 
This station is designed for a precise position measurement (alignment) before the multiple scattering in 
the calorimeters. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\subsection{Trigger system}

In general, particle physics experiments are searching for events that have a relatively low probability to be produced. 
Collisions at the interaction point inside the \lhcb detector occur at a rate of 40\mhz. 
Due to the limited computing facilities, the event rate that can be recorded is around a few \khz. 
The trigger system is designed to reduce the rate by rejecting background events. 
It decides whether events occurring in the detector should be stored or not.
This is done in several stages:

\begin{minipage}[H]{0.60\textwidth}
    \begin{itemize}
        \item \textbf{The Level-0 (L0) trigger} is a hardware-based trigger. It rapidly selects events that have muons or high transverse energy particles from collisions by using information from the VELO, the calorimeters, and the muon chambers. This reduces the readout rate down to 1\mhz. 
        \item \textbf{The High Level Trigger 1 (HLT1)} is the first stage of the software-based triggers. The HLT1 partially reconstructs events where a few tracks are chosen based on their transverse momenta and impact parameters (IP). This stage reduces the event rate further to around 50 \khz. 
        \item \textbf{The High Level Trigger 2 (HLT2)} is the second stage of the software-based triggers. After a positive decision of HLT1, data is transferred to disk with detector calibrations applied. At this stage, a full event reconstruction of tracks and vertices is performed. Events are categorised and selected based on their matching criteria with the topology of decay channels of interest. The event rate is reduced to 12.5 \khz.   
    \end{itemize}
\end{minipage}\hfil
\begin{minipage}[H]{0.35\textwidth}
  \centering
    \raisebox{-0.42\height}{%
  \includegraphics[width=\textwidth]{LHCb_Trigger_RunII_May2015.pdf}
    }
    \captionof{figure}{Flowchart of the trigger system in the Run 2 data-taking period~\cite{triggerSch}.}
	\label{Fig:TriggerSch}
\end{minipage}

\cref{Fig:TriggerSch} illustrates the event rate reduction from the collision rate to the storage rate.
The funtionalities of each trigger level are shown in~\cref{Fig:TrigDec}.
\begin{figure}[!b]
	\centering
		\includegraphics[width=0.78\textwidth]{TrigDec.pdf} 
    \caption{Trigger selections at each stage~\cite{LHCb-DP-2008-001}.}
	\label{Fig:TrigDec}
\end{figure}

